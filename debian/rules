#!/usr/bin/make -f
## ----------------------------------------------------------------------
## debian/rules : package script for debian-reference
## ----------------------------------------------------------------------


## ----------------------------------------------------------------------
## uncomment this to turn on verbose mode
#export DH_VERBOSE=1
## no-parallel to build with the cleaner log (it slows!)
#BUILDOPT:=	 --parallel
BUILDOPT:=	 --no-parallel

## ----------------------------------------------------------------------
# Package building language choice
# (This should be updated with the maintainer approval)
#
MANUAL	:=	debian-reference
LANGALL	:=	en ja fr it pt de zh-cn zh-tw es
LANGPO	:=	   ja fr it pt de zh-cn zh-tw es
# languages to skip generation of PDF files (not used now)
#NOPDF	:=	zh-cn zh-tw
NOPDF	:=	

## ----------------------------------------------------------------------
## Targets

%:
	dh $@

override_dh_auto_build:
	# Generate files for debhelper in debian/
	cd $(CURDIR)/debian/ ; \
	cp all.preinst-in   $(MANUAL)-common.preinst ; \
	cp all.postinst-in  $(MANUAL)-common.postinst ; \
	for XX in $(LANGALL) ; do \
	  cp all.preinst-in   $(MANUAL)-$${XX}.preinst ; \
	  cp all.postinst-in  $(MANUAL)-$${XX}.postinst ; \
	  cp all.postrm-in    $(MANUAL)-$${XX}.postrm ; \
	  case $$XX in \
	    en) LANGX="English" ;;  \
	    de) LANGX="German" ;;   \
	    fr) LANGX="French" ;;   \
	    fi) LANGX="Finnish" ;;  \
	    es) LANGX="Spanish" ;;  \
	    it) LANGX="Italian" ;;  \
	    ja) LANGX="Japanese" ;; \
	    pl) LANGX="Polish" ;; \
	    pt) LANGX="Portuguese (Portugal)" ;; \
	    pt-br) LANGX="Portuguese (Brazil)" ;; \
	    zh-tw) LANGX="Chinese (Traditional)" ;; \
	    zh-cn) LANGX="Chinese (Simplified)" ;; \
	    id) LANGX="Bahasa Indonesia" ;; \
	    *) LANGX="$$x" ;; \
	  esac ; \
	  sed  -e "s/@@/$$XX/g" < all.links-in  >$(MANUAL)-$${XX}.links ; \
	  sed  -e "s/@@/$$XX/g" < all.install-in  >$(MANUAL)-$${XX}.install ; \
	  sed  -e "s/@@/$$XX/g" -e "s/@LANG@/$$LANGX/g" < all.doc-base-in  >$(MANUAL)-$${XX}.doc-base ; \
	  sed  -e "s/@@/$$XX/g" < all.lintian-overrides-in >$(MANUAL)-$${XX}.lintian-overrides ; \
	done
	dh_auto_build $(BUILDOPT) -- \
		"LANGALL=$(LANGALL)" \
		"LANGPO=$(LANGPO)" \
		"NOPDF=$(NOPDF)" \
		all


override_dh_compress:
	dh_compress -X.pdf

override_dh_auto_test:
	: # no need to test build html pages

override_dh_auto_clean:
	# Clean files for debhelper in debian/
	-rm -f $(MANUAL)-common.preinst  $(MANUAL)-common.postinst
	-rm -f $(addsuffix .preinst,  $(addprefix  debian/$(MANUAL)-, $(LANGALL)))
	-rm -f $(addsuffix .postinst, $(addprefix  debian/$(MANUAL)-, $(LANGALL)))
	-rm -f $(addsuffix .postrm,   $(addprefix  debian/$(MANUAL)-, $(LANGALL)))
	-rm -f $(addsuffix .links,    $(addprefix  debian/$(MANUAL)-, $(LANGALL)))
	-rm -f $(addsuffix .doc-base, $(addprefix  debian/$(MANUAL)-, $(LANGALL)))
	-rm -f $(addsuffix .install,  $(addprefix  debian/$(MANUAL)-, $(LANGALL)))
	$(MAKE) "LANGALL=$(LANGALL)" "LANGPO=$(LANGPO)"  clean

